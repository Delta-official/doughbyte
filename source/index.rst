.. Doughbyte documentation master file, created by
   sphinx-quickstart on Tue Jan 26 21:12:49 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Doughbyte's documentation!
=====================================

.. toctree::
   welcome

.. toctree::
   :caption: Art

   art/e1d5bf6
   art/bdf0924
   art/6e045e8
   art/149d9d1
   art/225f935
   art/b7bc0d7
